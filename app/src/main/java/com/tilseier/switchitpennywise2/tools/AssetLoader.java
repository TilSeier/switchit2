package com.tilseier.switchitpennywise2.tools;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;

import com.tilseier.switchitpennywise2.R;

import java.util.Random;

/**
 * Created by TilSeier on 06.12.2017.
 */

public class AssetLoader extends Application {

    private static AudioAttributes audioAttributes;
    final static int SOUND_POOL_MAX = 5;

    private static MediaPlayer horrorAmbience;

    private static SoundPool soundPool;
    private static int nextSlideSound;
    private static int screamSound;
    private static int clickSound;
//    private static int laughSound;

    private static int laughSound1;
    private static int laughSound2;
    private static int laughSound3;
    private static int laughSound4;
    private static int laughSound5;

    private static Typeface font1;
    private static Typeface font2;
    private static Typeface font3;

//    public static Typeface scaryFont;
//    public static Typeface scaryFont2;

    public static boolean isLoaded = false;

    private static GamePreferences gamePreferences;

    public static void loadAssets(Context context){

        if (!isLoaded) {

            gamePreferences = new GamePreferences(context);

            //SoundPool is deprecated in API level 21. (Lollipop)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                audioAttributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_GAME)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build();

                soundPool = new SoundPool.Builder()
                        .setAudioAttributes(audioAttributes)
                        .setMaxStreams(SOUND_POOL_MAX)
                        .build();

            } else {
                soundPool = new SoundPool(SOUND_POOL_MAX, AudioManager.STREAM_MUSIC, 0);
            }
            //BackUP "D:\Android\Projects Source\Pennywise Switch IT\IT\app\src\main\res\raw"
            nextSlideSound = soundPool.load(context, R.raw.next_slide_1, 1);//4//5//6!//10~// //click_3!
            screamSound = soundPool.load(context, R.raw.scream_1, 1);
            clickSound = soundPool.load(context, R.raw.click_7, 1);//2//5//6//7!  //next_slide_3

            laughSound1 = soundPool.load(context, R.raw.laugh_1, 1);
            laughSound2 = soundPool.load(context, R.raw.laugh_2, 1);
            laughSound3 = soundPool.load(context, R.raw.laugh_3, 1);
            laughSound4 = soundPool.load(context, R.raw.laugh_4, 1);
            laughSound5 = soundPool.load(context, R.raw.laugh_5, 1);

            horrorAmbience = MediaPlayer.create(context, R.raw.horror_ambience_1);//1!//4//7//8!//9!
            horrorAmbience.setLooping(true);

            //FONTS
            font1 = Typeface.createFromAsset(context.getAssets(), "fonts/DK-Face-Your-Fears.otf");
            font2 = Typeface.createFromAsset(context.getAssets(), "fonts/shlop rg.ttf");
            font3 = Typeface.createFromAsset(context.getAssets(), "fonts/MYSCARS_.TTF");

            //RU: "fonts/DK-Face-Your-Fears.otf" or "Story-Brush.otf" //EN: "fonts/shlop rg.ttf"
            //RU: "fonts/DK-Face-Your-Fears.otf" //EN: "fonts/MYSCARS_.TTF"
            //Gypsy Curse//csnpwdt NFI//youmurdererbb_reg//terror_pro//MYSCARS_.TTF//MYBLS___//Ghastly Panic//

            isLoaded = true;

        }

    }

    public static Typeface getScaryFont(){
        if (gamePreferences.getCurrentLanguage().equals("ru"))
            return font1;
        else
            return font2;
    }

    public static Typeface getScaryFont2(){
        if (gamePreferences.getCurrentLanguage().equals("ru"))
            return font1;
        else
            return font3;
    }

    public static Typeface getScaryFont3(){
        return font1;
    }

    public static void playHorrorAmbience(){
        if (!horrorAmbience.isPlaying())
            horrorAmbience.start();
    }

    public static void pauseHorrorAmbience(){
        if (horrorAmbience.isPlaying())
            horrorAmbience.pause();

//        horrorAmbience.release();
    }

    public static void stopHorrorAmbience(){
        if (horrorAmbience.isPlaying()) {
            horrorAmbience.pause();
            horrorAmbience.seekTo(0);
        }
    }

    public static void playNextSlideSound(){
        soundPool.play(nextSlideSound, 1.0f,1.0f,1, 0,1.0f);
    }

    public static void playScreamSound(){
        soundPool.play(screamSound, 1.0f,1.0f,1, 0,1.0f);
    }

    public static void playClickSound(){
        Log.i("ClickSound", ""+clickSound);
        soundPool.play(clickSound, 1.0f,1.0f,1, 0,1.0f);
    }

    public static void playLaughSound(){

        Random rand = new Random();
        int indexLaugh = rand.nextInt(5)+1;

        if (indexLaugh == 1){
            soundPool.play(laughSound1, 1.0f,1.0f,1, 0,1.0f);
        }else if (indexLaugh == 2){
            soundPool.play(laughSound2, 1.0f,1.0f,1, 0,1.0f);
        }else if (indexLaugh == 3){
            soundPool.play(laughSound3, 1.0f,1.0f,1, 0,1.0f);
        }else if (indexLaugh == 4){
            soundPool.play(laughSound4, 1.0f,1.0f,1, 0,1.0f);
        }else if (indexLaugh == 5){
            soundPool.play(laughSound5, 1.0f,1.0f,1, 0,1.0f);
        }else {
            soundPool.play(laughSound1, 1.0f,1.0f,1, 0,1.0f);
        }

    }

}
