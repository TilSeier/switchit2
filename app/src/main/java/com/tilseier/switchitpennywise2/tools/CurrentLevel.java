package com.tilseier.switchitpennywise2.tools;

public class CurrentLevel {

    private static int CURRENT_LEVEL = 1;
    private static int ALL_LEVELS = 21;

    private static int[] levelScaryPictures = {12,14,16,18,18,20,20,22,22,24,24,26,26,28,30,32,34,36,38,40,40};
    private static int[] levelNormalPictures = {6,7,8,9,9,10,10,11,11,12,12,13,13,14,15,16,17,18,19,20,20};

    public static int getLevelScaryPictures() {
        return levelScaryPictures[CURRENT_LEVEL-1];
    }
    public static int getLevelNormalPictures() {
        return levelNormalPictures[CURRENT_LEVEL-1];
    }

    public static int getCurrentLevel() {
        return CURRENT_LEVEL;
    }
    public static void setCurrentLevel(int lvl) {
        CURRENT_LEVEL = lvl;
    }

    public static int getAllLevels() {
        return ALL_LEVELS;
    }

}