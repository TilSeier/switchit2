package com.tilseier.switchitpennywise2.tools;

import android.content.Context;
import android.content.SharedPreferences;

public class GamePreferences {

    private String INT_PASS_LEVELS = "enableLevels";
    private String INT_USELESS_WEB_BANNER = "countOfUselessWebBanner";
    private String STRING_LANGUAGE = "gameLanguage";
    private String BOOLEAN_SHOW_AD = "isShowAd";
    private String BOOLEAN_PRIVACY_POLICY_ACCEPTED = "privacy_policy_accepted";

    private SharedPreferences sPref;
    private SharedPreferences.Editor prefEditor;

    public GamePreferences(Context context){
        sPref = context.getSharedPreferences("com.tilseier.switchitpennywise2", Context.MODE_PRIVATE);
        prefEditor = sPref.edit();
    }

    public String getCurrentLanguage() {
        return sPref.getString(STRING_LANGUAGE, "en");
    }

    public int getPassLevels() {
        return sPref.getInt(INT_PASS_LEVELS, 0);
    }

    public int getCountOfUselessWebBanner() {
        return sPref.getInt(INT_USELESS_WEB_BANNER, 0);
    }

    public boolean getIsShowAd() {
        return sPref.getBoolean(BOOLEAN_SHOW_AD, false);
    }


    public void setCurrentLanguage(String lang) {
        prefEditor.putString(STRING_LANGUAGE, lang).apply();
    }

    public void setPassLevel() {
        prefEditor.putInt(INT_PASS_LEVELS, getPassLevels()+1).apply();
    }

    public void setCountOfUselessWebBanner(int count) {
        prefEditor.putInt(INT_USELESS_WEB_BANNER, count)
                .apply();
    }

    public void setIsShowAd() {
        prefEditor.putBoolean(BOOLEAN_SHOW_AD, !getIsShowAd()).apply();
    }


    public boolean isPrivacyPolicyAccepted(){
        return sPref.getBoolean(BOOLEAN_PRIVACY_POLICY_ACCEPTED, false);
    }

    public void setPrivacyPolicyAccepted(boolean b){
        prefEditor.putBoolean(BOOLEAN_PRIVACY_POLICY_ACCEPTED, b)
                .apply();
    }

}
