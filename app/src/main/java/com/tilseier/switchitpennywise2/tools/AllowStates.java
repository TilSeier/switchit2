package com.tilseier.switchitpennywise2.tools;

import android.os.Handler;

public class AllowStates {

    private static boolean isShowAds = false;
    private static boolean isShowRate = false;
    private static boolean isAdTimer = false;
    private static int SPLASH_TIME_OUT = 60000 * 3;

    public static boolean isStateTimer(){
        return isAdTimer;
    }

    public static void startStateTimer(){
        if (!isAdTimer){
            isAdTimer = true;

            new Handler().postDelayed(new Runnable(){
                @Override
                public void run()
                {
                    isShowAds = true;
                    isAdTimer = false;

                    isShowRate = true;//!isShowRate

                }
            }, SPLASH_TIME_OUT);
        }
    }

    public static boolean isShowAds(){
        return isShowAds;
    }

    public static void setShowAds(boolean b){
        isShowAds = b;
    }

    public static boolean isShowRate(){
        return isShowRate;
    }

}
