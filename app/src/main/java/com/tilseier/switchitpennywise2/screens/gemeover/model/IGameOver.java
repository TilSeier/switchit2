package com.tilseier.switchitpennywise2.screens.gemeover.model;

/**
 * Created by TilSeier on 09.03.2018.
 */

public interface IGameOver {

    String[] getWinPhrase();
    String[] getLoseSwitchPhrase();
    String[] getTimeOutPhrase();
    String[] getWinGamePhrase();

}
