package com.tilseier.switchitpennywise2.screens.game.model;

import android.util.Log;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class GameModel implements IGame {

    private int currentImgNum = 0;
    private boolean currentIsScary = false;
    private int prevImgNum;
    private boolean prevIsScary;

    private String prefixIsScary;

    private Random rand;
    private float currentRandFloat;

    private int currentIndexImg = 0;

    private Integer[] shuffledImages;

    private int currentLvl;
    private int currentNormalLvl;
    private int currentScaryLvl;

    private int currentLocation = 0;
    private int currentScaryLocations = 0;

//    public enum GameOverState { WIN, TIMEOUT, RUN, STAY };
    public static GameOverState currentGameState = GameOverState.TIMEOUT;

    private boolean isGameOver = false;

    private boolean isContinue = false;

    public GameModel(int currentLvl, int currentNormalLvl, int currentScaryLvl){
        rand = new Random();
        this.currentLvl = currentLvl;
        this.currentNormalLvl = currentNormalLvl;
        this.currentScaryLvl = currentScaryLvl;

        shuffledImages = shuffleImages(currentScaryLvl);
    }

    private Integer[] shuffleImages(int lvlCount) {

        Integer[] shuffledImg = new Integer[lvlCount];
        for (int i = 0; i < shuffledImg.length; i++) {
            shuffledImg[i] = i + 1;
        }
        Collections.shuffle(Arrays.asList(shuffledImg));
        System.out.println(Arrays.toString(shuffledImg));

        return shuffledImg;

    }

    @Override
    public String generateImage() {

        prevImgNum = currentImgNum;
        prevIsScary = currentIsScary;

        if (!(currentLocation < 2) && !isGameOver) {

            currentRandFloat = rand.nextFloat();

            currentIsScary = !(currentRandFloat <= 0.49);

        }else{
            currentIsScary = false;
        }


        if (isContinue){
            currentIsScary = false;
            isContinue = false;
        }


        if (currentIsScary){
            currentImgNum = shuffledImages[currentIndexImg];
            currentIndexImg++;
        }else {
            currentImgNum = rand.nextInt(currentNormalLvl)+1;
        }
        currentLocation++;

        while (currentImgNum == prevImgNum && !currentIsScary){
            currentImgNum = rand.nextInt(currentNormalLvl)+1;
        }

        prefixIsScary = currentIsScary ? "n" : "y";


        return prefixIsScary+"_img_"+currentImgNum+"_lvl_"+currentLvl;
    }

    @Override
    public boolean getIsScary() {
        return currentIsScary;
    }

    @Override
    public boolean getIsGameOver() {
        return isGameOver;
    }

    @Override
    public GameOverState getGameState() {
        return currentGameState;
    }

    @Override
    public void setGameState(GameOverState state) {
        currentGameState = state;
    }

    @Override
    public void continueGame() {

        if(currentGameState != GameOverState.RUN) {
            currentIndexImg--;
            if (currentIndexImg < 0)
                currentIndexImg = 0;
        }

//        System.out.println(Arrays.toString(shuffledImg));
//        Log.i("GLOBAL STEP", "indexImg="+indexImg);

        isGameOver = false;

        isContinue = true;

    }

    @Override
    public void setIsGameOver(boolean gameOver) {
        isGameOver = gameOver;
    }

    @Override
    public int getCurrentLocation() {
        return currentLocation;
    }

    @Override
    public int getCurrentScaryLocation() {
        return currentScaryLocations;
    }

    @Override
    public void setCurrentScaryLocation() {
        currentScaryLocations++;
    }


}
