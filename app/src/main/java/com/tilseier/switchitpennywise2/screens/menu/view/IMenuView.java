package com.tilseier.switchitpennywise2.screens.menu.view;

/**
 * Created by TilSeier on 26.03.2018.
 */

public interface IMenuView {

    void markLevels(int passLvl, int allLvl);

    void goToRules();
    void goBack();

}
