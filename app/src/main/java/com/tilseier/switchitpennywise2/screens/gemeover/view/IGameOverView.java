package com.tilseier.switchitpennywise2.screens.gemeover.view;

/**
 * Created by TilSeier on 26.03.2018.
 */

public interface IGameOverView {


    void playClickSound();
    void showNextLevel();
    void hideNextLevel();
    void showGameNote();
    void hideGameNote();
    void showGameOverText();
    void hideGameOverText();
    void showGameOverSubText();
    void hideGameOverSubText();
    void showLoserLover();
    void hideLoserLover();
    void setGameOverText(int strID);
    void setGameOverText(String str);
    void setGameOverSubText(int strID);
    void setGameOverSubText(String str);
    void goReplayLevel();
    void goToNextLevel();
    void goToMenu();
    void showAdAndGo(final Class goToClass);

}
