package com.tilseier.switchitpennywise2.screens.menu.presenter;

/**
 * Created by TilSeier on 09.03.2018.
 */

public interface IMenuPresenter {

    void onStart();
    void onLevelClick(int lvl);
    void onBackClick();

}
