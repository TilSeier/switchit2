package com.tilseier.switchitpennywise2.screens.gemeover;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.tilseier.switchitpennywise2.R;
import com.tilseier.switchitpennywise2.ads.AdmobApplication;
import com.tilseier.switchitpennywise2.screens.gemeover.model.GameOverModel;
import com.tilseier.switchitpennywise2.screens.gemeover.model.IGameOver;
import com.tilseier.switchitpennywise2.screens.gemeover.presenter.GameOverPresenter;
import com.tilseier.switchitpennywise2.screens.gemeover.presenter.IGameOverPresenter;
import com.tilseier.switchitpennywise2.screens.gemeover.view.IGameOverView;
import com.tilseier.switchitpennywise2.screens.menu.MenuActivity;
import com.tilseier.switchitpennywise2.screens.rules.RulesActivity;
import com.tilseier.switchitpennywise2.tools.AllowStates;
import com.tilseier.switchitpennywise2.tools.AssetLoader;
import com.tilseier.switchitpennywise2.tools.GamePreferences;

public class GameOverActivity extends AppCompatActivity implements IGameOverView {

    private IGameOverPresenter gameOverPresenter;

    private LinearLayout gameOverNote;
    private Button nextLevelButton;
    private ImageView loserLover;
    private TextView gameOverText;
    private TextView gameOverSubText;

    private Animation showTextAnim;
    private Animation showTextAnim2;
    private Animation showTextAnim3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game_over);
//        getSupportActionBar().hide();

        //banner view
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        AssetLoader.loadAssets(this);

        gameOverText = findViewById(R.id.gameOverText);
        gameOverSubText = findViewById(R.id.gameOverSubText);
        TextView warningText = findViewById(R.id.gameOverWarningText);

        gameOverNote = findViewById(R.id.gameOverNote);

        Button replayButton = findViewById(R.id.replayButtonID);
        Button menuButton = findViewById(R.id.menuButtonID);
        nextLevelButton = findViewById(R.id.nextLevelButton);

        loserLover = findViewById(R.id.loserLover);

        replayButton.setTypeface(AssetLoader.getScaryFont2());
        menuButton.setTypeface(AssetLoader.getScaryFont2());
        gameOverText.setTypeface(AssetLoader.getScaryFont());
        gameOverSubText.setTypeface(AssetLoader.getScaryFont());
        warningText.setTypeface(AssetLoader.getScaryFont());

        nextLevelButton.setTypeface(AssetLoader.getScaryFont());

        showTextAnim = AnimationUtils.loadAnimation(this, R.anim.show_text_anim);
        showTextAnim2 = AnimationUtils.loadAnimation(this, R.anim.show_text_anim);
        showTextAnim3 = AnimationUtils.loadAnimation(this, R.anim.show_text_anim);

        IGameOver gameOverModel = new GameOverModel(this);
        gameOverPresenter = new GameOverPresenter(this, gameOverModel, new GamePreferences(this));
        gameOverPresenter.onStart();

        replayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameOverPresenter.onReplayClick();
            }
        });

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameOverPresenter.onMenuClick();
            }
        });

        nextLevelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gameOverPresenter.onNextClick();
            }
        });

    }

    @Override
    public void playClickSound() {
        AssetLoader.playClickSound();
    }

    @Override
    public void showNextLevel() {
        nextLevelButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNextLevel() {
        nextLevelButton.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showGameNote() {
        gameOverNote.setVisibility(View.VISIBLE);
        gameOverNote.startAnimation(showTextAnim3);
    }

    @Override
    public void hideGameNote() {
        gameOverNote.setVisibility(View.GONE);
    }

    @Override
    public void showGameOverText() {
        gameOverText.setVisibility(View.VISIBLE);
        gameOverText.startAnimation(showTextAnim);
    }

    @Override
    public void hideGameOverText() {

    }

    @Override
    public void showGameOverSubText() {
        gameOverSubText.setVisibility(View.VISIBLE);
        gameOverSubText.startAnimation(showTextAnim2);
    }

    @Override
    public void hideGameOverSubText() {

    }

    @Override
    public void showLoserLover() {
        Animation loserLoverAnim = AnimationUtils.loadAnimation(this, R.anim.alpha_anim);

        loserLover.startAnimation(loserLoverAnim);
        loserLoverAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                loserLover.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                loserLover.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void hideLoserLover() {
        loserLover.setVisibility(View.GONE);
    }

    @Override
    public void setGameOverText(int strID) {
        gameOverText.setText(strID);
    }

    @Override
    public void setGameOverText(String str) {
        gameOverText.setText(str);
    }

    @Override
    public void setGameOverSubText(int strID) {
        gameOverSubText.setText(strID);
    }

    @Override
    public void setGameOverSubText(String str) {
        gameOverSubText.setText(str);
    }

    @Override
    public void goReplayLevel() {
        Intent intent = new Intent(this, RulesActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void goToNextLevel() {
        Intent intent = new Intent(this, RulesActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void goToMenu() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void showAdAndGo(Class goToClass) {

        if (AllowStates.isShowAds()){
            if (AdmobApplication.isAdLoaded()){

                Intent intent = new Intent(getApplicationContext(), goToClass);
                startActivity(intent);
                this.finish();

                AdmobApplication.displayLoadedAd();

                AdmobApplication.mInterstitialAd.setAdListener(new AdListener(){
                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        AllowStates.setShowAds(false);
                        AllowStates.startStateTimer();
                        AdmobApplication.requestNewInterstitial();
                    }
                });

            }else {
                AdmobApplication.requestNewInterstitial();

                Intent intent = new Intent(getApplicationContext(), goToClass);
                startActivity(intent);
                this.finish();
            }
        }else {
            Intent intent = new Intent(getApplicationContext(), goToClass);
            startActivity(intent);
            this.finish();
        }

    }

    @Override
    public void onBackPressed() {
        gameOverPresenter.onBackPressed();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
////        AssetLoader.playHorrorAmbience();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
////        AssetLoader.pauseHorrorAmbience();
//    }

}
