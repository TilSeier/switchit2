package com.tilseier.switchitpennywise2.screens.game.presenter;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.animation.DecelerateInterpolator;

import com.tilseier.switchitpennywise2.screens.game.model.GameModel;
import com.tilseier.switchitpennywise2.screens.game.model.IGame;
import com.tilseier.switchitpennywise2.screens.game.view.IGameView;
import com.tilseier.switchitpennywise2.screens.menu.MenuActivity;
import com.tilseier.switchitpennywise2.screens.slash.presenter.ISlashPresenter;
import com.tilseier.switchitpennywise2.screens.slash.view.ISlashView;
import com.tilseier.switchitpennywise2.tools.AllowStates;
import com.tilseier.switchitpennywise2.tools.CurrentLevel;
import com.tilseier.switchitpennywise2.tools.GamePreferences;

import java.util.Random;

/**
 * Created by TilSeier on 09.03.2018.
 */

public class GamePresenter implements IGamePresenter {

    private IGameView view;
    private IGame game;
    private CountDownTimer timer;
    private static int ROUND_TIME_OUT = 3100;
    private Random rand;

    private boolean isPause = false;

    //Video Reward Ads
    private boolean rewardContinue = false;
    private boolean usedRewardContinue = false;
    private boolean stageContinue = false;

    //Continue Timer Line
    private ObjectAnimator animationContinueTimer;
    private long mCurrentPlayTimeContinueTimer = 0;
    private boolean isAnimationContinueTimerCanceled = false;

    public GamePresenter(IGameView view){
        this.view = view;
        game = new GameModel(CurrentLevel.getCurrentLevel(), CurrentLevel.getLevelNormalPictures(), CurrentLevel.getLevelScaryPictures());
        rand = new Random();
    }

    @Override
    public void onStart() {

//        if (!view.isVideoLoaded())
//            view.loadRewardedVideoAd();

        randomImage();

    }

    private void randomImage(){

//        if (view.isVideoLoaded()){
//            Log.i("GLOBAL REWARD", "LOADED");
//        }else {
//            Log.i("GLOBAL REWARD", "NO_LOADED");
//        }


        view.updateScore(game.getCurrentScaryLocation(), CurrentLevel.getLevelScaryPictures());

        if (game.getCurrentScaryLocation() == CurrentLevel.getLevelScaryPictures()){
            gameOver(IGame.GameOverState.WIN);
            return;
        }

        //Change Image
        view.changeImage(game.generateImage());

        if (timer != null){
            timer.cancel();
            timer = null;
        }

        view.updateTime(3);

        if (!game.getIsGameOver()) {

            timer = new CountDownTimer(ROUND_TIME_OUT, 1000) {

                @Override
                public void onTick(long l) {

//                    Log.i("Timer: ", String.valueOf(l / 1000) + "s");

//                    timerText.setText(String.valueOf(l / 1000) + "s");

                    view.updateTime((int) (l / 1000));

                }

                @Override
                public void onFinish() {

//                    timerText.setText("0s");

                    view.updateTime(0);

                    if (game.getIsScary()) {
                        gameOver(IGame.GameOverState.TIMEOUT);
                    } else {
                        view.playNextSlideSound();

                        randomImage();
                    }

                }

            }.start();

        }

    }

    private void gameOver(IGame.GameOverState state) {

//        isGameOver = true;
        game.setIsGameOver(true);

        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        game.setGameState(state);

        if (!game.getGameState().equals(IGame.GameOverState.WIN)){
            view.showClown();
            view.playScreamSound();
        }

        // START NEW ACTIVITY ONE SECOND AFTER SCARY ANIM

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                if (!game.getGameState().equals(IGame.GameOverState.WIN) && !usedRewardContinue && game.getCurrentScaryLocation() >= 5 && view.isVideoLoaded()){
                    continueGameScreen();
                }else {
                    endGame();
                }

            }
        },1000);

        //END OF HANDLER
    }

    private void startAnimationContinueTimer() {
        animationContinueTimer.start();
        animationContinueTimer.setCurrentPlayTime(mCurrentPlayTimeContinueTimer);
    }

    private void stopAnimationContinueTimer(){
        mCurrentPlayTimeContinueTimer = animationContinueTimer.getCurrentPlayTime();
        animationContinueTimer.cancel();
    }

    private void continueGameScreen(){

        view.showContinueStage();
        stageContinue = true;

//        currentScoreText.setText(Integer.toString(currentScaryLocations) + "/" + Integer.toString(countsScaryLocations[level-1]));

//        currentScoreText.setText(getString(R.string.text_pass_count, currentScaryLocations, countsScaryLocations[level-1]));

        view.updateContinueScore(game.getCurrentScaryLocation(), CurrentLevel.getLevelScaryPictures());


        //Animation with Interval
        final Handler h = new Handler();
        h.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
//                continueButton.startAnimation(shakeTwoAnim);
                view.shakeContinueButton();
                h.postDelayed(this, 3000);
            }
        }, 1000); // 1 second delay (takes millis)

        //Continue Timer Line
        animationContinueTimer = ObjectAnimator.ofInt(view.getProgressLine(), "progress", 100, 0);
        animationContinueTimer.setDuration(10000);
        animationContinueTimer.setInterpolator(new DecelerateInterpolator());
        animationContinueTimer.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                isAnimationContinueTimerCanceled = false;
            }

            @Override
            public void onAnimationEnd(Animator animator) {
//                Log.i("GLOBAL STEP", "EndAnimation");
                //do something when the countdown is complete
                if (!isAnimationContinueTimerCanceled)
                    endGame();
            }

            @Override
            public void onAnimationCancel(Animator animator) {
//                Log.i("GLOBAL STEP", "CancelAnimation");
                isAnimationContinueTimerCanceled = true;
            }

            @Override
            public void onAnimationRepeat(Animator animator) { }
        });
        startAnimationContinueTimer();
//        animationContinueTimer.start();

    }

    private void continueGame(){
        view.hideContinueStage();

        stageContinue = false;

        game.continueGame();

        randomImage();

        rewardContinue = false;
    }

    private void endGame(){
        view.goToGameOver();
    }

    @Override
    public void onSwitchClick() {

        if(!game.getIsGameOver()) {

            if (!game.getIsScary()) {
                game.setIsGameOver(true);
                view.playNextSlideSound();

//                view.playLaughSound();

//                randomImage();

                view.shakeImage();

                if (timer != null){
                    timer.cancel();
                    timer = null;
                }

//                levelText.setText(Integer.toString(currentScaryLocations + 1) + "/" + Integer.toString(countsScaryLocations[level-1]));

//                levelText.setText(getString(R.string.text_pass_count, currentScaryLocations + 1, countsScaryLocations[level-1]));

                timer = new CountDownTimer(2800, 380) {

                    @Override
                    public void onTick(long l) {

                        view.playLaughSound();

//                        Log.i("Timer: ", String.valueOf((l + 1000) / 1000) + "s");

//                        timerText.setText(String.valueOf((l + 1000) / 1000) + "s");
//                        levelText.setText(Integer.toString(rand.nextInt(100)) + "/" + Integer.toString(rand.nextInt(100)));

//                        levelText.setText(getString(R.string.text_pass_count, rand.nextInt(100), rand.nextInt(100)));

                        view.updateScore(rand.nextInt(100), rand.nextInt(100));


//                        timerText.setText(String.valueOf(rand.nextInt(1000)) + "s");

                        view.updateTime(rand.nextInt(1000));

//                        timerText.setText(getString(R.string.text_timer, rand.nextInt(1000)));

                    }

                    @Override
                    public void onFinish() {
                        gameOver(IGame.GameOverState.RUN);
                    }

                }.start();


            } else {
                view.playLaughSound();
                view.playNextSlideSound();
                game.setCurrentScaryLocation();
                randomImage();
            }

        }else {
            if (!stageContinue) {
                view.playNextSlideSound();
                view.shakeImage();
                view.shakeSwitchButton();
            }
        }

    }

    @Override
    public void onContinueShow() {

    }

    @Override
    public void onContinueClick() {
        mCurrentPlayTimeContinueTimer = 0;
        animationContinueTimer.cancel();

        if (view.isVideoLoaded())
            view.showVideo();
        else
            view.loadRewardedVideoAd();
    }

    @Override
    public void onNoContinueClick() {
        mCurrentPlayTimeContinueTimer = 0;
        animationContinueTimer.cancel();
        endGame();
    }

    @Override
    public void onBackClick() {

        if(!game.getIsGameOver()) {
            if (timer != null){
                timer.cancel();
                timer = null;
            }

//            view.goBack();
            view.showAdAndGo(MenuActivity.class);

//            showAdsAndGo(GameMenuActivity.class);

        }else {
            if (!stageContinue) {

//                AssetLoader.playNextSlideSound();
//            backButton.startAnimation(shakeAnim);

                view.playNextSlideSound();

//                view.shakeImage();
                view.shakeBackButton();
//                view.shakeSwitchButton();

            }

        }

    }

    @Override
    public void onBackPressed() {
        if(!game.getIsGameOver()) {
            if (timer != null){
                timer.cancel();
                timer = null;
            }
            if (stageContinue)
                stopAnimationContinueTimer();

//            showAdsAndGo(GameMenuActivity.class);
//            view.goBack();
            view.showAdAndGo(MenuActivity.class);

        }else {
            if (!stageContinue) {

                view.shakeImage();
                view.shakeSwitchButton();
                view.shakeBackButton();

            }
        }
    }

    @Override
    public void onResume() {
//        AssetLoader.playHorrorAmbience();

//        Log.i("onResume", "GAME " + isPause);

        if (!rewardContinue) {

            if (!stageContinue) {

                if (game.getIsGameOver() && isPause) {
                    gameOver(IGame.GameOverState.RUN);
                } else {
                    if (isPause) {
//                        view.goBack();
                        view.showAdAndGo(MenuActivity.class);
                        isPause = false;
                    }
                }

            }else {
                startAnimationContinueTimer();
            }

        } else {
            continueGame();
        }
    }

    @Override
    public void onPause() {
//        AssetLoader.pauseHorrorAmbience();

        if (timer != null){
            timer.cancel();
            timer = null;
        }
        if (stageContinue)
            stopAnimationContinueTimer();

        isPause = true;

//        Log.i("onPause", "GAME");
    }

    @Override
    public void onRewardedVideoAdClosed() {
        if (!rewardContinue) {
            endGame();
        }
//        rewardContinue = false;
        Log.i("GLOBAL STEP", "onClosed");
    }

    @Override
    public void onRewarded() {
        rewardContinue = true;
        usedRewardContinue = true;
        Log.i("GLOBAL STEP", "onReward");
    }

}
