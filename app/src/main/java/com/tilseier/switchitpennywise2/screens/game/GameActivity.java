package com.tilseier.switchitpennywise2.screens.game;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.tilseier.switchitpennywise2.R;
import com.tilseier.switchitpennywise2.ads.AdmobApplication;
import com.tilseier.switchitpennywise2.screens.game.presenter.GamePresenter;
import com.tilseier.switchitpennywise2.screens.game.presenter.IGamePresenter;
import com.tilseier.switchitpennywise2.screens.game.view.IGameView;
import com.tilseier.switchitpennywise2.screens.gemeover.GameOverActivity;
import com.tilseier.switchitpennywise2.screens.menu.MenuActivity;
import com.tilseier.switchitpennywise2.tools.AssetLoader;

public class GameActivity extends AppCompatActivity implements IGameView, RewardedVideoAdListener {

    private IGamePresenter gamePresenter;

    private ImageView gamePicture;
    private ImageView scaryPicture;
    private TextView timerText;
    private TextView levelText;
    private Button buttonSwitch;
    private Button buttonBack;

    //Animation
    private Animation blinkPicture;
    private Animation shakeAnim;
    private Animation shakeAnim2;
    private Animation shakeAnim3;
    private Animation shakeTwoAnim;

    //Continue Function
    private ConstraintLayout continueLayout;
    private Button buttonContinue;
    private Button buttonNoContinue;
    private TextView textContinue;//gameOverText.setTypeface(AssetLoader.scaryFont);
    private TextView textCurrentScore;
//    private boolean isContinue = false;

    //Video Reward Ads
    private RewardedVideoAd mRewardedVideoAd;
//    private static final String mobileAdsAppID = "ca-app-pub-3940256099942544~3347511713";
    private static final String rewardAdID = "ca-app-pub-7508352159228516/6027949087";

    //Continue Timer Line
    private ProgressBar mProgressBarContinueTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game);
//        getSupportActionBar().hide();

        //Interstitial
        AdmobApplication.createWallAd(this);
        AdmobApplication.requestNewInterstitial();

        //banner view
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        //BBBBBBBBBBBBBBBBAAAAAAAAAAAAAAAAAAAAAAGGGGGGGGGGGGGGGGGG
        //Video Reward Ads
//        MobileAds.initialize(this, mobileAdsAppID);
        // Use an activity context to get the rewarded video instance.
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
        loadRewardedVideoAd();

        AssetLoader.loadAssets(this);

        gamePicture = findViewById(R.id.gamePicture);
        scaryPicture = findViewById(R.id.scaryPicture);
        timerText = findViewById(R.id.timerText);
        levelText = findViewById(R.id.levelText);
        buttonBack = findViewById(R.id.backButtonGame);
        buttonSwitch = findViewById(R.id.runButtonID);

        //Continue Function
        continueLayout = findViewById(R.id.continueLayout);
        buttonContinue = findViewById(R.id.continueButton);
        buttonNoContinue = findViewById(R.id.noContinueButton);
        textContinue = findViewById(R.id.continueText);
        textCurrentScore = findViewById(R.id.currentScoreText);

        //Typeface
        timerText.setTypeface(AssetLoader.getScaryFont2());
        levelText.setTypeface(AssetLoader.getScaryFont2());
        buttonBack.setTypeface(AssetLoader.getScaryFont2());
        buttonSwitch.setTypeface(AssetLoader.getScaryFont());

        //Continue Function Typeface
        textContinue.setTypeface(AssetLoader.getScaryFont());
        textCurrentScore.setTypeface(AssetLoader.getScaryFont());
        buttonContinue.setTypeface(AssetLoader.getScaryFont2());
        buttonNoContinue.setTypeface(AssetLoader.getScaryFont2());

        //Animation
        blinkPicture = AnimationUtils.loadAnimation(this, R.anim.blink_anim);
        shakeAnim = AnimationUtils.loadAnimation(this, R.anim.shake_anim);
        shakeAnim2 = AnimationUtils.loadAnimation(this, R.anim.shake_anim);
        shakeAnim3 = AnimationUtils.loadAnimation(this, R.anim.shake_anim);
        shakeTwoAnim = AnimationUtils.loadAnimation(this, R.anim.shake_two_anim);

        //Continue Timer Line
        mProgressBarContinueTimer = findViewById(R.id.progressBarContinueTimer);

        gamePresenter = new GamePresenter(this);
        gamePresenter.onStart();

        buttonSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gamePresenter.onSwitchClick();
            }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gamePresenter.onBackClick();
            }
        });

        buttonContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gamePresenter.onContinueClick();
            }
        });

        buttonNoContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gamePresenter.onNoContinueClick();
            }
        });

    }

    @Override
    public void loadRewardedVideoAd() {
        if (!mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.loadAd(rewardAdID,
                    new AdRequest.Builder()
                            .build());
//.addTestDevice("EC07F4759620B8F1E3BD5F493490BEB4")
//.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
        }
    }

    @Override
    public boolean isVideoLoaded() {
        return mRewardedVideoAd.isLoaded();
    }

    @Override
    public void showVideo() {
        mRewardedVideoAd.show();
    }

    @Override
    public void playLaughSound() {
        AssetLoader.playLaughSound();
    }

    @Override
    public void playNextSlideSound() {
        AssetLoader.playNextSlideSound();
    }

    @Override
    public void playScreamSound() {
        AssetLoader.playScreamSound();
    }

    @Override
    public ProgressBar getProgressLine() {
        return mProgressBarContinueTimer;
    }

    @Override
    public void showClown() {

        AssetLoader.stopHorrorAmbience();
        //SCARY ANIMATION
        Animation scaryClown = AnimationUtils.loadAnimation(this, R.anim.scary_anim);
        scaryPicture.startAnimation(scaryClown);
        //END OF SCARY ANIMATION
//        assetLoader.playScreamSound();// SCREAM SOUND

    }

    @Override
    public void changeImage(String imgName) {

//        if ( gamePicture.getDrawable() instanceof BitmapDrawable) {
//            ((BitmapDrawable)gamePicture.getDrawable()).getBitmap().recycle();
//        }
//        gamePicture.getDrawable().setCallback(null);
//        gamePicture.setImageDrawable(null);

//        gamePicture.getResources().flushLayoutCache();
//        gamePicture.destroyDrawingCache();

        Log.i("GLOBAL IMG", imgName);

        int resID = getResources().getIdentifier(imgName, "drawable", getPackageName());

        gamePicture.startAnimation(blinkPicture);

        gamePicture.setImageResource(resID);

    }

    @Override
    public void shakeImage() {
        gamePicture.startAnimation(shakeAnim);
    }

    @Override
    public void shakeBackButton() {
        buttonBack.startAnimation(shakeAnim2);
    }

    @Override
    public void shakeSwitchButton() {
        buttonSwitch.startAnimation(shakeAnim3);
    }

    @Override
    public void shakeContinueButton() {
        buttonContinue.startAnimation(shakeTwoAnim);
    }

    @Override
    public void updateTime(int time) {
        timerText.setText(getString(R.string.text_timer, time));
    }

    @Override
    public void updateScore(int s1, int s2) {
        levelText.setText(getString(R.string.text_pass_count, s1, s2));
    }

    @Override
    public void updateContinueScore(int s1, int s2) {
        textCurrentScore.setText(getString(R.string.text_pass_count, s1, s2));
    }

    @Override
    public void updateTimeLine(int t) {

    }

    @Override
    public void showContinueStage() {
        continueLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideContinueStage() {
        continueLayout.setVisibility(View.GONE);
    }

    @Override
    public void goBack() {

        gamePicture.getResources().flushLayoutCache();
        gamePicture.destroyDrawingCache();

        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void goToGameOver() {

        gamePicture.getResources().flushLayoutCache();
        gamePicture.destroyDrawingCache();

        Intent intent = new Intent(this, GameOverActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void showAdAndGo(Class goToClass) {

        gamePicture.getResources().flushLayoutCache();
        gamePicture.destroyDrawingCache();

        if (AdmobApplication.isAdLoaded()){

            Intent intent = new Intent(getApplicationContext(), goToClass);
            startActivity(intent);
            this.finish();

            AdmobApplication.displayLoadedAd();

            AdmobApplication.mInterstitialAd.setAdListener(new AdListener(){
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    AdmobApplication.requestNewInterstitial();
                }
            });

        }else {
            AdmobApplication.requestNewInterstitial();

            Intent intent = new Intent(getApplicationContext(), goToClass);
            startActivity(intent);
            this.finish();
        }

    }

    @Override
    public void onBackPressed() {
        gamePresenter.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AssetLoader.playHorrorAmbience();
        gamePresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AssetLoader.pauseHorrorAmbience();
        gamePresenter.onPause();
    }

    @Override
    public void onRewardedVideoAdLoaded() {

    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {
        gamePresenter.onRewardedVideoAdClosed();
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        gamePresenter.onRewarded();
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {

    }

    @Override
    public void onRewardedVideoCompleted() {

    }
}
