package com.tilseier.switchitpennywise2.screens.slash.presenter;

import android.os.Handler;

import com.tilseier.switchitpennywise2.screens.home.HomeActivity;
import com.tilseier.switchitpennywise2.screens.slash.view.ISlashView;
import com.tilseier.switchitpennywise2.tools.AllowStates;
import com.tilseier.switchitpennywise2.tools.GamePreferences;

/**
 * Created by TilSeier on 09.03.2018.
 */

public class SlashPresenter implements ISlashPresenter {

    private ISlashView view;
    private GamePreferences gamePreferences;
    private static int SPLASH_TIME_OUT = 4000;

    public SlashPresenter(ISlashView view, GamePreferences gamePreferences){
        this.view = view;
        this.gamePreferences = gamePreferences;
    }

    @Override
    public void onStart() {

        view.hideDialogPrivacyPolicy();

        if(gamePreferences.isPrivacyPolicyAccepted())
            initApp();
        else
            view.showDialogPrivacyPolicy();

    }

    @Override
    public void onDialogPrivacyPolicyOpenClick() {
        view.goToPrivacyPolicy();
    }

    @Override
    public void onDialogPrivacyPolicyAcceptClick() {
        gamePreferences.setPrivacyPolicyAccepted(true);
        view.hideDialogPrivacyPolicy();
        initApp();
    }

    private void initApp() {
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run()
            {

                view.setLocale();
                view.showAdAndGo(HomeActivity.class);

            }
        }, SPLASH_TIME_OUT);
    }

}
