package com.tilseier.switchitpennywise2.screens.rules;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.tilseier.switchitpennywise2.R;
import com.tilseier.switchitpennywise2.screens.game.GameActivity;
import com.tilseier.switchitpennywise2.screens.menu.MenuActivity;
import com.tilseier.switchitpennywise2.screens.rules.presenter.IRulesPresenter;
import com.tilseier.switchitpennywise2.screens.rules.presenter.RulesPresenter;
import com.tilseier.switchitpennywise2.screens.rules.view.IRulesView;
import com.tilseier.switchitpennywise2.tools.AssetLoader;

public class RulesActivity extends AppCompatActivity implements IRulesView{

    private IRulesPresenter rulesPresenter;
    private TextView levelTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_rules);
//        getSupportActionBar().hide();

        //banner view
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        AssetLoader.loadAssets(this);

        Button backButton = findViewById(R.id.backButtonRules);
        Button startButton = findViewById(R.id.startButtonRules);

        TextView title1 = findViewById(R.id.title1);
        TextView title2 = findViewById(R.id.title2);

        levelTextView = findViewById(R.id.levelTextView);

        //Typeface
        backButton.setTypeface(AssetLoader.getScaryFont2());
        startButton.setTypeface(AssetLoader.getScaryFont());
        title1.setTypeface(AssetLoader.getScaryFont());
        title2.setTypeface(AssetLoader.getScaryFont());
        levelTextView.setTypeface(AssetLoader.getScaryFont());

        rulesPresenter = new RulesPresenter(this);
        rulesPresenter.onStart();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rulesPresenter.onBackClick();
            }
        });

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rulesPresenter.onStartClick();
            }
        });

    }

    @Override
    public void setLevel(int lvl) {
        levelTextView.setText(getString(R.string.level, lvl));
    }

    @Override
    public void goToGame() {
        AssetLoader.playClickSound();
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void goBack() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void onBackPressed() {
        rulesPresenter.onBackClick();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AssetLoader.playHorrorAmbience();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AssetLoader.pauseHorrorAmbience();
    }

}
