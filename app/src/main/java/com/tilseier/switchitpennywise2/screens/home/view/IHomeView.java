package com.tilseier.switchitpennywise2.screens.home.view;

/**
 * Created by TilSeier on 26.03.2018.
 */

public interface IHomeView {

    void setLocale(String lang);
    void showRate();
    void hideRate();
    void showOwnAdBanner();
    void hideOwnAdBanner();
    void showShareIcon();
    void hideShareIcon();
    void shareGame();
    void refreshActivity();
    void goToMenu();
    void goToRateGame();
    void goToInstallUselessWebGame();

}
