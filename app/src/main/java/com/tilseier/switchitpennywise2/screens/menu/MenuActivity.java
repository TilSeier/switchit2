package com.tilseier.switchitpennywise2.screens.menu;

import android.content.Intent;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.tilseier.switchitpennywise2.R;
import com.tilseier.switchitpennywise2.screens.home.HomeActivity;
import com.tilseier.switchitpennywise2.screens.menu.presenter.IMenuPresenter;
import com.tilseier.switchitpennywise2.screens.menu.presenter.MenuPresenter;
import com.tilseier.switchitpennywise2.screens.menu.view.IMenuView;
import com.tilseier.switchitpennywise2.screens.rules.RulesActivity;
import com.tilseier.switchitpennywise2.tools.AssetLoader;
import com.tilseier.switchitpennywise2.tools.GamePreferences;

public class MenuActivity extends AppCompatActivity implements IMenuView {

    private Button buttonBack;
    private IMenuPresenter menuPresenter;

    private GamePreferences gamePreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_menu);
//        getSupportActionBar().hide();

        //banner view
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        AssetLoader.loadAssets(this);

        buttonBack = findViewById(R.id.backButtonMenu);

        //Typeface
        buttonBack.setTypeface(AssetLoader.getScaryFont2());

        gamePreferences = new GamePreferences(this);

        menuPresenter = new MenuPresenter(this, gamePreferences);
        menuPresenter.onStart();

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuPresenter.onBackClick();
            }
        });
    }

    public void chooseLevel(View view){

        menuPresenter.onLevelClick(Integer.parseInt(view.getTag().toString()));

    }

    @Override
    public void markLevels(int passLvl, int allLvl) {

        for (int i = 0; i < (passLvl+1) && i < allLvl; i++){
            int resID = getResources().getIdentifier("level"+(i+1), "id", getPackageName());
            Button unlockLevel = (Button) findViewById(resID);
            unlockLevel.setEnabled(true);
            if (passLvl == allLvl || (i+1) <= passLvl)
                setButtonBackgroundColor(unlockLevel, R.color.colorBloodPass);
            else
                setButtonBackgroundColor(unlockLevel, R.color.colorBlood);
            unlockLevel.setTextColor(getResources().getColor(R.color.colorWhite));
            unlockLevel.setTypeface(AssetLoader.getScaryFont());
        }

    }

    private void setButtonBackgroundColor(Button button, int colorBG) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP && button instanceof AppCompatButton) {
            ((AppCompatButton) button).setBackgroundColor(getResources().getColor(colorBG));
        } else {
            ViewCompat.setBackground(button, getResources().getDrawable(colorBG));
        }
    }

    @Override
    public void goToRules() {
        AssetLoader.playClickSound();
        Intent intent = new Intent(this, RulesActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void goBack() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void onBackPressed() {
        menuPresenter.onBackClick();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AssetLoader.playHorrorAmbience();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AssetLoader.pauseHorrorAmbience();
    }

}
