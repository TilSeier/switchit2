package com.tilseier.switchitpennywise2.screens.slash.presenter;

/**
 * Created by TilSeier on 09.03.2018.
 */

public interface ISlashPresenter {

    void onStart();
    void onDialogPrivacyPolicyOpenClick();
    void onDialogPrivacyPolicyAcceptClick();

}
