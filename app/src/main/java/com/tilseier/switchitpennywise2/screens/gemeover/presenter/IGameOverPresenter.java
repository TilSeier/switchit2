package com.tilseier.switchitpennywise2.screens.gemeover.presenter;

/**
 * Created by TilSeier on 09.03.2018.
 */

public interface IGameOverPresenter {

    void onStart();
    void onReplayClick();
    void onMenuClick();
    void onNextClick();
    void onBackPressed();

}
