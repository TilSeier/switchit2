package com.tilseier.switchitpennywise2.screens.slash.view;

/**
 * Created by TilSeier on 26.03.2018.
 */

public interface ISlashView {

    void goToPrivacyPolicy();
    void showDialogPrivacyPolicy();
    void hideDialogPrivacyPolicy();
    void showAdAndGo(final Class goToClass);
    void setLocale();

}
