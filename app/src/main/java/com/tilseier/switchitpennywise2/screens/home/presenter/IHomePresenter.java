package com.tilseier.switchitpennywise2.screens.home.presenter;

/**
 * Created by TilSeier on 09.03.2018.
 */

public interface IHomePresenter {

    void onStart();
    void onPlayClick();
    void onLanguageClick();
    void onRateClick();
    void onShareClick();
    void onBannerInstallClick();
    void onBannerCloseClick();

}
