package com.tilseier.switchitpennywise2.screens.game.model;

/**
 * Created by TilSeier on 09.03.2018.
 */

public interface IGame {

    enum GameOverState { WIN, TIMEOUT, RUN, STAY };

    String generateImage();
    boolean getIsScary();
    boolean getIsGameOver();
    GameOverState getGameState();
    void setGameState(GameOverState state);

    void continueGame();

    void setIsGameOver(boolean gameOver);
    int getCurrentLocation();
    int getCurrentScaryLocation();
    void setCurrentScaryLocation();

}
