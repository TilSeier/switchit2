package com.tilseier.switchitpennywise2.screens.menu.presenter;

import android.os.Handler;

import com.tilseier.switchitpennywise2.screens.menu.view.IMenuView;
import com.tilseier.switchitpennywise2.screens.slash.presenter.ISlashPresenter;
import com.tilseier.switchitpennywise2.screens.slash.view.ISlashView;
import com.tilseier.switchitpennywise2.tools.AllowStates;
import com.tilseier.switchitpennywise2.tools.CurrentLevel;
import com.tilseier.switchitpennywise2.tools.GamePreferences;

/**
 * Created by TilSeier on 09.03.2018.
 */

public class MenuPresenter implements IMenuPresenter {

    private IMenuView view;
    private GamePreferences gamePreferences;

    public MenuPresenter(IMenuView view, GamePreferences gamePreferences){
        this.view = view;
        this.gamePreferences = gamePreferences;
    }

    @Override
    public void onStart() {

        view.markLevels(gamePreferences.getPassLevels(), CurrentLevel.getAllLevels());

    }

    @Override
    public void onLevelClick(int lvl) {

        CurrentLevel.setCurrentLevel(lvl);
        view.goToRules();

    }

    @Override
    public void onBackClick() {
        view.goBack();
    }
}
