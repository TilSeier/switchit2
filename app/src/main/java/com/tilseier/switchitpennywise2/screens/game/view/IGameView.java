package com.tilseier.switchitpennywise2.screens.game.view;

import android.widget.ProgressBar;

/**
 * Created by TilSeier on 26.03.2018.
 */

public interface IGameView {

    void loadRewardedVideoAd();
    boolean isVideoLoaded();
    void showVideo();
    void playLaughSound();
    void playNextSlideSound();
    void playScreamSound();
    ProgressBar getProgressLine();
    void showClown();
    void changeImage(String imgName);
    void shakeImage();
    void shakeBackButton();
    void shakeSwitchButton();
    void shakeContinueButton();
    void updateTime(int t);
    void updateScore(int s1, int s2);
    void updateContinueScore(int s1, int s2);
    void updateTimeLine(int t);
    void showContinueStage();
    void hideContinueStage();
    void goBack();
    void goToGameOver();
    void showAdAndGo(final Class goToClass);

}
