package com.tilseier.switchitpennywise2.screens.slash;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tilseier.switchitpennywise2.R;
import com.tilseier.switchitpennywise2.ads.AdmobApplication;
import com.tilseier.switchitpennywise2.screens.slash.presenter.ISlashPresenter;
import com.tilseier.switchitpennywise2.screens.slash.presenter.SlashPresenter;
import com.tilseier.switchitpennywise2.screens.slash.view.ISlashView;
import com.tilseier.switchitpennywise2.tools.AllowStates;
import com.tilseier.switchitpennywise2.tools.AssetLoader;
import com.tilseier.switchitpennywise2.tools.GamePreferences;

import java.util.Locale;

public class SlashActivity extends AppCompatActivity implements ISlashView {

    final String PRIVACY_POLICY_LINK = "https://sites.google.com/view/switch-it-2-by-koval-maksym";

    private FirebaseAnalytics mFirebaseAnalytics;

    private GamePreferences gamePreferences;
    private ISlashPresenter slashPresenter;

    private ConstraintLayout clDialogPrivacyPolicyWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_slash);
//        getSupportActionBar().hide();

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        //Interstitial
        AdmobApplication.createWallAd(this);
        AdmobApplication.requestNewInterstitial();

        AssetLoader.loadAssets(this);

        clDialogPrivacyPolicyWrapper = findViewById(R.id.layout_dialog_privacy_policy_wrapper);

        TextView textLoading = findViewById(R.id.loadingTextView);
        textLoading.setTypeface(AssetLoader.getScaryFont3());

        gamePreferences = new GamePreferences(this);

        slashPresenter = new SlashPresenter(this, new GamePreferences(this));
        slashPresenter.onStart();

    }

    public void onDialogPrivacyPolicyOpenClick(View view){
        slashPresenter.onDialogPrivacyPolicyOpenClick();
    }

    public void onDialogPrivacyPolicyAcceptClick(View view){
        slashPresenter.onDialogPrivacyPolicyAcceptClick();
    }

    @Override
    public void goToPrivacyPolicy() {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(PRIVACY_POLICY_LINK));
        startActivity(intent);
    }

    @Override
    public void showDialogPrivacyPolicy() {

        Animation animScaleIn = AnimationUtils.loadAnimation(this, R.anim.scale_anim);

        clDialogPrivacyPolicyWrapper.startAnimation(animScaleIn);
        clDialogPrivacyPolicyWrapper.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideDialogPrivacyPolicy() {

        Animation animScaleOut = AnimationUtils.loadAnimation(this, R.anim.scale_out_anim);

        clDialogPrivacyPolicyWrapper.startAnimation(animScaleOut);
        clDialogPrivacyPolicyWrapper.setVisibility(View.GONE);
    }

    @Override
    public void showAdAndGo(Class goToClass) {
        if (gamePreferences.getIsShowAd() && AdmobApplication.isAdLoaded()){

            gamePreferences.setIsShowAd();

            //Finish the splash activity so it can't be returned to.
            Intent intent = new Intent(this, goToClass);
            startActivity(intent);
            this.finish();

            AdmobApplication.displayLoadedAd();

            AdmobApplication.mInterstitialAd.setAdListener(new AdListener(){
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    AdmobApplication.requestNewInterstitial();
                    AllowStates.startStateTimer();
                }
            });

        } else {

            AdmobApplication.requestNewInterstitial();
            AllowStates.startStateTimer();

            gamePreferences.setIsShowAd();

            //Finish the splash activity so it can't be returned to.
            Intent intent = new Intent(this, goToClass);
            startActivity(intent);
            this.finish();

        }
    }

    @Override
    public void setLocale() {
        Locale myLocale = new Locale(gamePreferences.getCurrentLanguage());
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

}
