package com.tilseier.switchitpennywise2.screens.rules.presenter;

/**
 * Created by TilSeier on 09.03.2018.
 */

public interface IRulesPresenter {

    void onStart();
    void onStartClick();
    void onBackClick();

}
