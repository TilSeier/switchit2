package com.tilseier.switchitpennywise2.screens.rules.view;

/**
 * Created by TilSeier on 26.03.2018.
 */

public interface IRulesView {

    void setLevel(int lvl);
    void goToGame();
    void goBack();

}
