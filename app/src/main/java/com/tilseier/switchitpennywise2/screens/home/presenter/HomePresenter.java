package com.tilseier.switchitpennywise2.screens.home.presenter;

import com.tilseier.switchitpennywise2.screens.home.view.IHomeView;
import com.tilseier.switchitpennywise2.tools.AllowStates;
import com.tilseier.switchitpennywise2.tools.GamePreferences;

/**
 * Created by TilSeier on 09.03.2018.
 */

public class HomePresenter implements IHomePresenter {

    private IHomeView view;
    private GamePreferences gamePreferences;
    private String CURRENT_LANG = "en";

    private final int COUNT_OF_SHOW_BANNER = 10;

    private static boolean isOwnAdBannerWasShown = false;

    public HomePresenter(IHomeView view, GamePreferences gamePreferences){
        this.view = view;
        this.gamePreferences = gamePreferences;
        CURRENT_LANG = gamePreferences.getCurrentLanguage();

        if (!isOwnAdBannerWasShown && gamePreferences.getCountOfUselessWebBanner() < COUNT_OF_SHOW_BANNER) {
//            showOwnAdBanner();
            view.showOwnAdBanner();
            isOwnAdBannerWasShown = true;
        }

    }

    @Override
    public void onStart() {

        view.showShareIcon();

        if (AllowStates.isShowRate()) {
            view.showRate();
//            view.showShareIcon();
        }else {
            view.hideRate();
//            view.hideShareIcon();
        }

    }

    @Override
    public void onPlayClick() {
        view.goToMenu();
    }

    @Override
    public void onLanguageClick() {

        if (CURRENT_LANG.equals("en")) {
            gamePreferences.setCurrentLanguage("ru");
            view.setLocale("ru");
        } else if (CURRENT_LANG.equals("ru")) {
            gamePreferences.setCurrentLanguage("es");
            view.setLocale("es");
        } else {
            gamePreferences.setCurrentLanguage("en");
            view.setLocale("en");
        }
        view.refreshActivity();

    }

    @Override
    public void onRateClick() {
        view.goToRateGame();
    }

    @Override
    public void onShareClick() {
        view.shareGame();
    }

    @Override
    public void onBannerInstallClick() {
        view.goToInstallUselessWebGame();
    }

    @Override
    public void onBannerCloseClick() {

        gamePreferences.setCountOfUselessWebBanner(gamePreferences.getCountOfUselessWebBanner()+1);

        view.hideOwnAdBanner();
    }
}
