package com.tilseier.switchitpennywise2.screens.home;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.tilseier.switchitpennywise2.R;
import com.tilseier.switchitpennywise2.ads.AdmobApplication;
import com.tilseier.switchitpennywise2.screens.home.presenter.HomePresenter;
import com.tilseier.switchitpennywise2.screens.home.presenter.IHomePresenter;
import com.tilseier.switchitpennywise2.screens.home.view.IHomeView;
import com.tilseier.switchitpennywise2.screens.menu.MenuActivity;
import com.tilseier.switchitpennywise2.tools.AssetLoader;
import com.tilseier.switchitpennywise2.tools.CurrentLevel;
import com.tilseier.switchitpennywise2.tools.GamePreferences;

import java.util.Locale;

public class HomeActivity extends AppCompatActivity implements IHomeView {

    private IHomePresenter homePresenter;

    private Button buttonRate;

    private ImageView ivShareGame;

    private ConstraintLayout clOwnAdBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_home);
//        getSupportActionBar().hide();

        AssetLoader.loadAssets(this);

        //Interstitial
        AdmobApplication.createWallAd(this);
        AdmobApplication.requestNewInterstitial();

        TextView mainText = findViewById(R.id.mainText);
        TextView subText = findViewById(R.id.subText);
        Button buttonPlay = findViewById(R.id.buttonPlay);
        Button buttonLenguage = findViewById(R.id.buttonLanguage);
        buttonRate = findViewById(R.id.buttonRate);
        ivShareGame = findViewById(R.id.iv_share_game);

        clOwnAdBanner = (ConstraintLayout) findViewById(R.id.adv_banner_game_useless_web);
        clOwnAdBanner.setVisibility(View.GONE);

        //Typeface
        mainText.setTypeface(AssetLoader.getScaryFont());
        subText.setTypeface(AssetLoader.getScaryFont());
        buttonPlay.setTypeface(AssetLoader.getScaryFont2());
        buttonLenguage.setTypeface(AssetLoader.getScaryFont2());
        buttonRate.setTypeface(AssetLoader.getScaryFont());

        GamePreferences gamePreferences = new GamePreferences(this);

        homePresenter = new HomePresenter(this, gamePreferences);

        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homePresenter.onPlayClick();
            }
        });

        buttonLenguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homePresenter.onLanguageClick();
            }
        });

        buttonRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homePresenter.onRateClick();
            }
        });

        ivShareGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homePresenter.onShareClick();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        homePresenter.onStart();
    }


    public void onBannerInstallClick(View view){

        homePresenter.onBannerInstallClick();

    }

    public void onBannerCloseClick(View view){

        homePresenter.onBannerCloseClick();

    }


    @Override
    public void setLocale(String lang) {
//        assetLoader.changeLocalTypeface(lang);

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    public void showRate() {
        buttonRate.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRate() {
        buttonRate.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showOwnAdBanner() {

        Animation animScaleIn = AnimationUtils.loadAnimation(this, R.anim.scale_anim);

        clOwnAdBanner.startAnimation(animScaleIn);
        clOwnAdBanner.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideOwnAdBanner() {

        Animation animScaleOut = AnimationUtils.loadAnimation(this, R.anim.scale_out_anim);

        clOwnAdBanner.startAnimation(animScaleOut);
        clOwnAdBanner.setVisibility(View.GONE);

    }

    @Override
    public void showShareIcon() {
        ivShareGame.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideShareIcon() {
        ivShareGame.setVisibility(View.INVISIBLE);
    }

    @Override
    public void shareGame() {

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = getString(R.string.shared_message);
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "For fans of IT :)");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));

    }


    @Override
    public void refreshActivity() {
        AssetLoader.playClickSound();
        Intent refresh = new Intent(this, HomeActivity.class);
        startActivity(refresh);
        this.finish();
    }

    @Override
    public void goToMenu() {
        AssetLoader.playClickSound();
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void goToRateGame() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=com.tilseier.switchitpennywise2")));
        }catch (ActivityNotFoundException e){
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=com.tilseier.switchitpennywise2")));
        }
    }

    @Override
    public void goToInstallUselessWebGame() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=com.tilseier.uselessweb")));
        }catch (ActivityNotFoundException e){
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=com.tilseier.uselessweb")));
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

//        if (AdmobApplication.isAdLoaded()){
//            AdmobApplication.displayLoadedAd();
//
//            this.finish();
//
//            AdmobApplication.mInterstitialAd.setAdListener(new AdListener(){
//                @Override
//                public void onAdClosed() {
//                    super.onAdClosed();
//                    AdmobApplication.requestNewInterstitial();
//                }
//            });
//
//        }else {
//            AdmobApplication.requestNewInterstitial();
//            this.finish();
//        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        AssetLoader.playHorrorAmbience();
    }

    @Override
    protected void onPause() {
        super.onPause();
        AssetLoader.pauseHorrorAmbience();
    }

}
