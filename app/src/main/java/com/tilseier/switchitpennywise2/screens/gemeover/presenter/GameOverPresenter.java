package com.tilseier.switchitpennywise2.screens.gemeover.presenter;

import android.os.Handler;

import com.tilseier.switchitpennywise2.R;
import com.tilseier.switchitpennywise2.screens.game.model.GameModel;
import com.tilseier.switchitpennywise2.screens.gemeover.model.IGameOver;
import com.tilseier.switchitpennywise2.screens.gemeover.view.IGameOverView;
import com.tilseier.switchitpennywise2.screens.home.HomeActivity;
import com.tilseier.switchitpennywise2.screens.menu.MenuActivity;
import com.tilseier.switchitpennywise2.screens.rules.RulesActivity;
import com.tilseier.switchitpennywise2.tools.CurrentLevel;
import com.tilseier.switchitpennywise2.tools.GamePreferences;

import java.util.Arrays;

/**
 * Created by TilSeier on 09.03.2018.
 */

public class GameOverPresenter implements IGameOverPresenter {

    private IGameOverView view;
    private IGameOver model;
    private GamePreferences gamePreferences;

    public GameOverPresenter(IGameOverView view, IGameOver model, GamePreferences gamePreferences){
        this.view = view;
        this.model = model;
        this.gamePreferences = gamePreferences;
    }

    @Override
    public void onStart() {
        view.hideGameNote();
        view.hideLoserLover();
        view.hideNextLevel();

        switch (GameModel.currentGameState){
            case TIMEOUT:

                String[] timeOutPhrase = model.getTimeOutPhrase();

                System.out.println(Arrays.toString(timeOutPhrase));

                if(timeOutPhrase.length >= 1 && timeOutPhrase[0] != null)
                    view.setGameOverText(timeOutPhrase[0]);
                if(timeOutPhrase.length >= 2 && timeOutPhrase[1] != null)
                    view.setGameOverSubText(timeOutPhrase[1]);

                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run()
                    {
                        view.showLoserLover();
                    }
                }, 4000);
                break;
            case RUN:

                String[] loseSwitchPhrase = model.getLoseSwitchPhrase();

                System.out.println(Arrays.toString(loseSwitchPhrase));

                if(loseSwitchPhrase.length >= 1 && loseSwitchPhrase[0] != null)
                    view.setGameOverText(loseSwitchPhrase[0]);
                if(loseSwitchPhrase.length >= 2 && loseSwitchPhrase[1] != null)
                    view.setGameOverSubText(loseSwitchPhrase[1]);

                new Handler().postDelayed(new Runnable(){
                    @Override
                    public void run()
                    {
                        view.showGameNote();
                        view.showLoserLover();
                    }
                }, 3000);
                break;
            case WIN:

                int currentLvl = CurrentLevel.getCurrentLevel();
                if (currentLvl == (gamePreferences.getPassLevels()+1)) {
                    gamePreferences.setPassLevel();
                }
                if (currentLvl != CurrentLevel.getAllLevels()){

                    String[] winPhrase = model.getWinPhrase();

                    if(winPhrase.length >= 1 && winPhrase[0] != null)
                        view.setGameOverText(winPhrase[0]);
                    if(winPhrase.length >= 2 && winPhrase[1] != null)
                        view.setGameOverSubText(winPhrase[1]);

                    view.showNextLevel();

                }else {

                    String[] winGamePhrase = model.getWinGamePhrase();
//                    view.setGameOverText(R.string.game_over_win_game);

                    if(winGamePhrase.length >= 1 && winGamePhrase[0] != null)
                        view.setGameOverText(winGamePhrase[0]);
                    if(winGamePhrase.length >= 2 && winGamePhrase[1] != null)
                        view.setGameOverSubText(winGamePhrase[1]);

                }
                break;
            default:
                break;
        }


        view.showGameOverText();
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run()
            {
                view.showGameOverSubText();
            }
        }, 2000);


    }

    @Override
    public void onReplayClick() {
//        view.goReplayLevel();
        view.showAdAndGo(RulesActivity.class);
    }

    @Override
    public void onMenuClick() {
//        view.goToMenu();
        view.showAdAndGo(MenuActivity.class);
    }

    @Override
    public void onNextClick() {
        CurrentLevel.setCurrentLevel(CurrentLevel.getCurrentLevel()+1);
//        view.goToNextLevel();
        view.playClickSound();
        view.showAdAndGo(RulesActivity.class);
    }

    @Override
    public void onBackPressed() {
//        view.goToMenu();
        view.showAdAndGo(MenuActivity.class);
    }
}
