package com.tilseier.switchitpennywise2.screens.rules.presenter;

import android.os.Handler;

import com.tilseier.switchitpennywise2.screens.rules.view.IRulesView;
import com.tilseier.switchitpennywise2.screens.slash.presenter.ISlashPresenter;
import com.tilseier.switchitpennywise2.screens.slash.view.ISlashView;
import com.tilseier.switchitpennywise2.tools.AllowStates;
import com.tilseier.switchitpennywise2.tools.CurrentLevel;
import com.tilseier.switchitpennywise2.tools.GamePreferences;

/**
 * Created by TilSeier on 09.03.2018.
 */

public class RulesPresenter implements IRulesPresenter {

    private IRulesView view;

    public RulesPresenter(IRulesView view){
        this.view = view;
    }

    @Override
    public void onStart() {
        view.setLevel(CurrentLevel.getCurrentLevel());
    }

    @Override
    public void onStartClick() {
        view.goToGame();
    }

    @Override
    public void onBackClick() {
        view.goBack();
    }
}
