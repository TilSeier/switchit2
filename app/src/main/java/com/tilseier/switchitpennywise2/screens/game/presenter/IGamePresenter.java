package com.tilseier.switchitpennywise2.screens.game.presenter;

/**
 * Created by TilSeier on 09.03.2018.
 */

public interface IGamePresenter {

    void onStart();
    void onSwitchClick();
    void onContinueShow();
    void onContinueClick();
    void onNoContinueClick();
    void onBackClick();
    void onBackPressed();
    void onResume();
    void onPause();
    void onRewardedVideoAdClosed();
    void onRewarded();

}
