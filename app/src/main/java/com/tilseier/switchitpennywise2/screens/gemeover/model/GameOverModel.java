package com.tilseier.switchitpennywise2.screens.gemeover.model;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.tilseier.switchitpennywise2.R;

import java.util.Arrays;
import java.util.Random;

public class GameOverModel implements IGameOver{

    private String[] winPhrases;
    private String[] loseSwitchPhrases;
    private String[] timeOutPhrases;
    private String[] winGamePhrases;

    private Random rand;

    public GameOverModel(Context context){

        Resources res = context.getResources();
        winPhrases = res.getStringArray(R.array.win_phrases_array);
        loseSwitchPhrases = res.getStringArray(R.array.lose_switch_phrases_array);
        timeOutPhrases = res.getStringArray(R.array.time_out_phrases_array);
        winGamePhrases = res.getStringArray(R.array.win_game_phrases_array);

        System.out.println(Arrays.toString(winPhrases));
        System.out.println(Arrays.toString(loseSwitchPhrases));
        System.out.println(Arrays.toString(timeOutPhrases));

        rand = new Random();

    }


    @Override
    public String[] getWinPhrase() {

        int phrase_index =  rand.nextInt(winPhrases.length);

        String[] res = winPhrases[phrase_index].split(";");

        System.out.println(Arrays.toString(res));

        Log.i("GLOBAL LENGTH", ""+res.length);

        return res;

    }

    @Override
    public String[] getLoseSwitchPhrase() {

        int phrase_index =  rand.nextInt(loseSwitchPhrases.length);

        String[] res = loseSwitchPhrases[phrase_index].split(";");

        System.out.println(Arrays.toString(res));

        Log.i("GLOBAL LENGTH", ""+res.length);

        return res;

    }

    @Override
    public String[] getTimeOutPhrase() {

        int phrase_index =  rand.nextInt(timeOutPhrases.length);

        String[] res = timeOutPhrases[phrase_index].split(";");

        System.out.println(Arrays.toString(res));

        Log.i("GLOBAL LENGTH", ""+res.length);

        return res;

    }

    @Override
    public String[] getWinGamePhrase() {

        int phrase_index =  rand.nextInt(winGamePhrases.length);

        String[] res = winGamePhrases[phrase_index].split(";");

        System.out.println(Arrays.toString(res));

        Log.i("GLOBAL LENGTH", ""+res.length);

        return res;

    }
}
